package com.company;
import java.util.*;
import  java.io.*;


public class Main {

    public static void main(String[] args) throws  Exception{

        // для проверки:
        ArrayList<Integer> inp = new ArrayList<>();
        inp.add(14);
        inp.add(-925);
        inp.add(8807);
        inp.add(3);
        inp.add(-8);
        inp.add(-1);

        System.out.println(Main.getMinNumberV1(inp));
        System.out.println(Main.getMinNumberV2());

    }


    public static  int getMinNumberV1(ArrayList<Integer> inputSequence) throws Exception // ввод последовательности в аргументах
    {
        //Превратим последовательность чисел в последовательность цифр, затем в последовательности цифр найдем минимальную

        StringBuilder sb = new StringBuilder();

        for(int i=0; i<inputSequence.size()-1; i++) {//судя по примеру в ДЗ "-1" не является частью последовательности, не берем последний элемент ("-1")
            String inputSequenceElementAsString = inputSequence.get(i).toString();
            String inputSequenceElementNumberOnly = inputSequenceElementAsString.replaceAll("[^0-9]", "");// очистим последовательность цифр от знаков "-" ( и знака "," если надо будет переделать функцию под работу с дробными числами)
            sb.append(inputSequenceElementNumberOnly);
        }

        String sequenceNumbers = sb.toString();
        char[] sequenceNumbersAsCharArray = sequenceNumbers.toCharArray();
        ArrayList<Integer> sequenceNumbersAsIntegerList = new ArrayList<>();

        for(char number: sequenceNumbersAsCharArray) {
            int a = Character.getNumericValue(number);
            sequenceNumbersAsIntegerList.add(a);
        }
        //int result = Collections.min(sequenceNumbersAsIntegerList); //сделаем поиск минимума вручную на случай если это подразумевалось заданием

        int result = 9;
        for(int a: sequenceNumbersAsIntegerList) {
            if(a<result) {
                result=a;
            }
        }

        return result;
    }


    public static  int getMinNumberV2() throws Exception // ввод последовательности через консоль
    {
        //Превратим последовательность чисел в последовательность цифр, затем в последовательности цифр найдем минимальную
        ArrayList<Integer> inputSequence = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Введите последовательность.");
        while(true) {
            String input = reader.readLine();
            Integer a = Integer.parseInt(input);
            if(input.equals("-1")){ //судя по примеру в ДЗ "-1" не является частью последовательности, не добавляем в обрабатываемый список
                break;
            }
            inputSequence.add(a);
        }

        StringBuilder sb = new StringBuilder();

        for(Integer inputSequenceElement: inputSequence) {
            String inputSequenceElementAsString = inputSequenceElement.toString();
            String inputSequenceElementNumberOnly = inputSequenceElementAsString.replaceAll("[^0-9]", "");// очистим последовательность цифр от знаков "-" ( и знака "," если надо будет переделать функцию под работу с дробными числами)
            sb.append(inputSequenceElementNumberOnly);
        }

        String sequenceNumbers = sb.toString();
        char[] sequenceNumbersAsCharArray = sequenceNumbers.toCharArray();
        ArrayList<Integer> sequenceNumbersAsIntegerList = new ArrayList<>();

        for(char number: sequenceNumbersAsCharArray) {
            int a = Character.getNumericValue(number);
            sequenceNumbersAsIntegerList.add(a);
        }

        //int result = Collections.min(sequenceNumbersAsIntegerList); //сделаем поиск минимума вручную на случай если это подразумевалось заданием

        int result = 9;
        for(int a: sequenceNumbersAsIntegerList) {
            if(a<result) {
                result=a;
            }
        }

        return result;
    }

}
